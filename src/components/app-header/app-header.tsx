import {Component, Event, EventEmitter, h, Prop} from '@stencil/core';
import {LogoutHttpService} from "../../http_services/logout.http_service";

@Component({
  tag: 'app-header',
  styleUrl: 'app-header.css'
})
export class AppHeader {
  @Prop() courseInfo: any = null;
  @Event() courseHeaderClicked: EventEmitter;

  private router = document.querySelector('ion-router');

  headerClicked() {
    if (this.courseInfo === null) {return;}
    this.courseHeaderClicked.emit();
  }

  async logoutButtonClicked() {
    await new LogoutHttpService().delete();
    localStorage.clear();
    this.router.push('/')
  }

  render() {
    return (
        <ion-header class="translucent" onClick={_ => this.headerClicked()}>
           <ion-toolbar>
             <ion-icon slot="start" class="logo" src="/../assets/img/logo.svg" />
             <ion-button style={{paddingRight:"20px"}} slot={'end'} onClick={() => {this.logoutButtonClicked()}} color={'primary'} >
               Logout
             </ion-button>
             {this.renderCourseInfo()}
           </ion-toolbar>
        </ion-header>
    );
  }


  renderCourseInfo() {
    if (this.courseInfo === null) {return;}
    return [
      <p style={{marginTop: '0px', marginBottom: '5px', fontWeight: 'bold'}} >
        {this.courseInfo.name}
      </p>,
      <p style={{marginTop: '0px', marginBottom: '5px'}}>
        {this.courseInfo.number} - {this.courseInfo.section}
      </p>
    ];
  }
}
