import { Component, h, State, Listen } from '@stencil/core';
import {LoginHttpService} from "../../http_services/login.http_service";
// import {LoginHttpService} from "../../http_services/login.http_service";

@Component({
  tag: 'app-login-page',
  styleUrl: 'app-login-page.css'
})
export class AppLoginPage {
  token: string;
  emailInput: HTMLIonInputElement;
  passwordInput: HTMLIonInputElement;
  @State() emailColor = 'medium';
  @State() passColor = 'medium';

  componentWillLoad() {
    if (localStorage.token !== undefined) {
      location.pathname = '/home/'
    }
  }

  async login() {
    try {
      let response = await new LoginHttpService().create({user:
          {email: this.emailInput.value, password: this.passwordInput.value}
      });
      if (response != null) {
        if (response.admin) {
          location.pathname = '/home/'
        } else {
          location.pathname = '/syllabus/'
        }
      }
    } catch (err) {
      alert("Invalid email or password. Please try again.");
      this.passwordInput.value = '';
    }
  }

  emailInputKeyPress(event) {
    if (event.key === 'Enter') {
      this.passwordInput.setFocus();
    }
  }

  passwordInputKeyPress(event) {
    if (event.key === 'Enter') {
      this.login();
    }
  }


  @Listen('ionFocus')
  yellowLabel(event){
    if (event.target.name === 'email') {
      this.emailColor = 'secondary'
    } else if (event.target.name === 'password') {
      this.passColor = 'secondary'
    }
  }

  @Listen('ionBlur')
  greyLabel(event){
    if (event.target.value === ''){
      if (event.target.name === 'email') {
        this.emailColor = 'medium'
      } else if (event.target.name === "password") {
        this.passColor = 'medium'
      }
    }
  }

  render() {
    return [
      <ion-content color="primary" text-center={true}>
        <ion-grid>
          <ion-row style={{marginTop: '25px'}}>
            <ion-col>
              <img src="../../assets/icon/Sillibi-Logo.svg" style={{marginBottom: '25px'}}/>
              <ion-item color="primary" class="center" style={{width: '330px'}}>
                <ion-label color={this.emailColor} position="floating">
                  Email Address
                </ion-label>
                <ion-input
                  name={'email'}
                  style={{borderBottom: "1px solid rgb(181, 181, 181, .2)", width: '300px'}}
                  ref={el => this.emailInput = el as HTMLIonInputElement}
                  onKeyPress={ event => this.emailInputKeyPress(event)}
                />
              </ion-item>
              <ion-item color="primary" class="center" style={{width: '330px'}}>
                <ion-label color={this.passColor} position="floating">
                  Password
                </ion-label>
                <ion-input name={'password'} type="password" ref={el => this.passwordInput = el as HTMLIonInputElement}
                  style={{borderBottom: "1px solid rgb(181, 181, 181, .2)", width: '300px'}}
                  onKeyPress={ event => this.passwordInputKeyPress(event)}
                />
              </ion-item>
            </ion-col>
          </ion-row>
          <ion-row style={{height:'15vh', marginTop: '5vh'}}>
            <ion-col>
              <ion-button style={{width: '300px'}} color="secondary"
                          onClick={() => { this.login()}}>
                LOGIN
              </ion-button>
            </ion-col>
          </ion-row>
          <ion-row class="ion-align-items-center" style={{height:'20vh'}}>
            <ion-col>
              <ion-label>
              <p>
                Don't have an account?
              </p>
              </ion-label>
              <ion-button href="/register">
                <ion-text color="tertiary">
                  REGISTER
                </ion-text>
              </ion-button>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
