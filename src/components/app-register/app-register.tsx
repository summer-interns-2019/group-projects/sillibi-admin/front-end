import {Component, h, Listen, State} from '@stencil/core';
import {RegisterHttpService} from "../../http_services/register.http_service";
import {LoginHttpService} from "../../http_services/login.http_service";

@Component({
  tag: 'app-register',
  styleUrl: 'app-register.css'
})
export class AppRegister {
  @State() firstColor = 'medium';
  @State() lastColor = 'medium';
  @State() emailColor = 'medium';
  @State() passColor = 'medium';
  firstNameInput: HTMLIonInputElement;
  lastNameInput: HTMLIonInputElement;
  workerIdInput: HTMLIonInputElement;
  emailInput: HTMLIonInputElement;
  passwordInput: HTMLIonInputElement;
  inputsArr() {
    return [this.firstNameInput, this.lastNameInput, this.workerIdInput, this.emailInput, this.passwordInput];
  }

  keyPressEvent(event, index) {
    if (event.key === 'Enter') {
      let inputs = this.inputsArr();
      if (index !== inputs.length - 1) {
        inputs[index + 1].setFocus();
      } else {
        this.register();
      }
    }
  }


  @Listen('ionFocus')
  yellowLabel(event){
    if (event.target.name === 'first') {
      this.firstColor = 'secondary'
    } else if (event.target.name === 'last') {
      this.lastColor = 'secondary'
    } else if (event.target.name === 'email') {
      this.emailColor = 'secondary'
    } else if (event.target.name === 'password') {
      this.passColor = 'secondary'
    }
  }

  @Listen('ionBlur')
  greyLabel(event){
    if (event.target.value === ''){
      if (event.target.name === 'first') {
        this.firstColor = 'medium'
      } else if (event.target.name === "last") {
        this.lastColor = 'medium'
      } else if (event.target.name === 'email') {
        this.emailColor = 'medium'
      } else if (event.target.name === "password") {
        this.passColor = 'medium'
      }
    }
  }

  async register() {
    console.log('Register!!');
    try {
      console.log(`Email: ${this.emailInput.value}. Password: ${this.passwordInput.value}`);
      const response = await new RegisterHttpService().create({user:
          {email: this.emailInput.value, password: this.passwordInput.value}
      });
      if (response.errors != null) {
        const error = response.errors[0].status;
        if (error === "400"){
          let allErrors = `email ${response.errors[0].detail.email}\npassword ${response.errors[0].detail.password}`;
          alert(allErrors)
        }
      } else {
        await new LoginHttpService().create({user: {email: this.emailInput.value, password: this.passwordInput.value}});
        // await new ProfileHttpService().create({first_name: this.firstName, last_name: this.lastName, email: user.email, user_id: response.id});
        location.pathname = '/syllabus'
      }
    } catch (err) {
      alert("Something went wrong. Please try again.")
    }
  }

  render() {
    return [
      <ion-content color="primary" text-center={true}>
        <ion-grid>
          <ion-row class="ion-align-items-end" style={{marginTop: '25px'}}>
            <ion-col>
              <img src="../../assets/icon/Sillibi-Logo.svg" style={{marginBottom: '25px'}}/>
              <ion-item color="primary" class="center" style={{width: '330px'}} >
                <ion-label color={this.firstColor} position="floating">
                  First Name
                </ion-label>
                <ion-input name='first' style={{borderBottom: "1px solid rgb(181, 181, 181, .2)", width: '300px'}}
                           ref={el => this.firstNameInput = el as HTMLIonInputElement}
                           onKeyPress={ event => this.keyPressEvent(event, 0)} />
              </ion-item>
              <ion-item color="primary" class="center" style={{width: '330px'}}>
                <ion-label color={this.lastColor} position="floating">
                  Last Name
                </ion-label>
                <ion-input name='last' ref={el => this.lastNameInput = el as HTMLIonInputElement}
                           style={{borderBottom: "1px solid rgb(181, 181, 181, .2)", width: '300px'}}
                           onKeyPress={ event => this.keyPressEvent(event, 1)} />
              </ion-item>
              <ion-item color="primary" class="center" style={{width: '330px'}}>
                <ion-label color={this.lastColor} position="floating">
                  MTurk Worker ID
                </ion-label>
                <ion-input name='last' ref={el => this.workerIdInput = el as HTMLIonInputElement}
                           style={{borderBottom: "1px solid rgb(181, 181, 181, .2)", width: '300px'}}
                           onKeyPress={event => this.keyPressEvent(event, 2)}/>
              </ion-item>
              <ion-item color="primary" class="center" style={{width: '330px'}}>
                <ion-label color={this.emailColor} position="floating">
                  Email Address
                </ion-label>
                <ion-input name='email' ref={el => this.emailInput = el as HTMLIonInputElement}
                           style={{borderBottom: "1px solid rgb(181, 181, 181, .2)", width: '300px'}}
                           onKeyPress={ event => this.keyPressEvent(event, 3)} />
              </ion-item>
              <ion-item color="primary" class="center" style={{width: '330px'}}>
                <ion-label color={this.passColor} position="floating">
                  Password
                </ion-label>
                <ion-input name={'password'} style={{borderBottom: "1px solid rgb(181, 181, 181, .2)", width: '300px'}}
                           type="password" ref={el => this.passwordInput = el as HTMLIonInputElement}
                           onKeyPress={ event => this.keyPressEvent(event, 4)} />
              </ion-item>
            </ion-col>
          </ion-row>
          <ion-row class="ion-align-items-center" style={{height:'15vh', marginTop: '5vh'}}>
            <ion-col>
              <ion-button color="secondary" style={{width: '300px'}} onClick={() => { this.register()}}>
                CREATE ACCOUNT
              </ion-button>
            </ion-col>
          </ion-row>
          <ion-row class="ion-align-items-center" style={{height:'20vh'}}>
            <ion-col>
              <ion-label>
                <p>
                  Already have an account?
                </p>
              </ion-label>
              <ion-button href="/login">
                <ion-text color="tertiary">
                  SIGN IN
                </ion-text>
              </ion-button>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
