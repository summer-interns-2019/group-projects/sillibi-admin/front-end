import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {

  render() {
    return (
      <ion-app>
        <ion-router useHash={false} >
          <ion-route url="/" component="app-landing" />
          <ion-route url="/login" component="app-login-page" />
          <ion-route url="/register" component="app-register" />
          <ion-route url="/syllabus/" component="syllabus-entry-page" />
          <ion-route url="/home" component="app-home" />
          <ion-route url="/profile/:name" component="app-profile" />
          <ion-route url="/syllabus-review/:syllabusId" component="syllabus-card-review" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
