import { Component, h, Prop, Event, EventEmitter} from '@stencil/core';

@Component({
  tag: 'assignment-form',
  styleUrl: 'assignment-form.css'
})

export class AssignmentForm {
  slides: HTMLIonSlidesElement;
  name: HTMLIonInputElement;
  possible_points: HTMLIonInputElement;
  due_date: HTMLIonInputElement;
  description: HTMLIonInputElement;
  inputsArr() { return [this.name, this.possible_points, this.due_date, this.description];}
  @Prop() assignmentInfo: any = null;
  @Event() assignmentAdded: EventEmitter;
  @Event() assignmentEdited: EventEmitter;

  addAssignment() {
    if (this.hasBlanks()) {
      return;
    } else {//Make DB Call First
      this.assignmentAdded.emit({
        name: this.name.value,
        possible_points: this.possible_points.value,
        due_date: this.due_date.value,
        description: this.description.value,
      })
    }
    {
      this.name.value = '',
      this.possible_points.value = '',
      this.due_date.value = '',
      this.description.value = ''
    }
    console.log(this.slides.getActiveIndex());
    this.slides.slideTo(0)
  }


  hasBlanks() {
    let checkArr = this.inputsArr();
    for (let i = 0; i < checkArr.length; i++) {
      if (checkArr[i].value === '') {
        return true;
      }
    }
    return false;
  }

  async keyPressEvent(event, index) {
    if (event.key === 'Enter') {
      let inputs = this.inputsArr();
      console.log( await this.slides.getActiveIndex())
      await this.slides.slideNext()
      console.log( await this.slides.getActiveIndex())
      if (index !== inputs.length) {
        inputs[index].setFocus();
      }
      this.addAssignment()
    }
  }

  makeAssignmentValObj() {
    let returnObj = {
      name: "",
      possible_points: "",
      due_date: "",
      description: ""
    };
    if (this.assignmentInfo != null) {
      returnObj = this.assignmentInfo;
    }
    return returnObj;
  }

  renderAssignmentForm() {
    let valObj = this.makeAssignmentValObj();
    return [
      <ion-row style={{height: '140px'}}>
        <ion-slides ref={el => this.slides = el as HTMLIonSlidesElement}>
          <ion-slide>
              <ion-card color="light" style={{width: '100%'}} class={"ion-padding"}>
                <ion-card-content>
                  <ion-label class="slide-label">
                    NAME
                  </ion-label>
                  <ion-input class="input-text"  placeholder={'Mid Term Essay'} ref={el => this.name = el as HTMLIonInputElement}
                           onKeyPress={ event => this.keyPressEvent(event, 0)} value={valObj.name}>
                  </ion-input>
                </ion-card-content>
              </ion-card>
          </ion-slide>
          <ion-slide>
            <ion-card color="light" style={{width: '100%'}} class={"ion-padding"}>
              <ion-card-content>
                <ion-label class="slide-label">POINTS
                </ion-label>
                <ion-input class="input-text" text-center={true}  placeholder={'10000'} ref={el => this.possible_points = el as HTMLIonInputElement}
                           onKeyPress={ event => this.keyPressEvent(event, 1)} value={valObj.possible_points}>
                </ion-input>
              </ion-card-content>
            </ion-card>
          </ion-slide>
          <ion-slide>
            <ion-card color="light" style={{width: '100%'}} class={"ion-padding"}>
              <ion-card-content>
                <ion-label class="slide-label">
                  DUE DATE
                </ion-label>
              <ion-input  class="input-text" text-center={true} placeholder={'12/12/2012'} ref={el => this.due_date = el as HTMLIonInputElement}
                         onKeyPress={ event => this.keyPressEvent(event, 2)} value={valObj.due_date}>
              </ion-input>
              </ion-card-content>
            </ion-card>
          </ion-slide>
          <ion-slide>
            <ion-card color="light" style={{width: '100%'}} class={"ion-padding"}>
            <ion-card-content>
              <ion-label class="slide-label">
                DESCRIPTION
              </ion-label>
              <ion-input class="input-text" text-center={true} placeholder={'The most important essay of your life...'} ref={el => this.description = el as HTMLIonInputElement}
                         onKeyPress={ event => this.keyPressEvent(event, 3)} value={valObj.description}>
              </ion-input>
            </ion-card-content>
            </ion-card>
          </ion-slide>
        </ion-slides>
      </ion-row>
    ];
  }

  render() {
    return [
      <ion-content color={'dark'}>
        <ion-toolbar color={'dark'}>
          <h3 class={"ion-padding"}> List Assignments From Silly Thing</h3>
        </ion-toolbar>
        <ion-grid>
          <ion-row>
            <ion-col size="1">
            </ion-col>
            <ion-col size="10">
              {this.renderAssignmentForm()}
            </ion-col>
            <ion-col size="1">
            </ion-col>
          </ion-row>
          <ion-footer>
            <ion-toolbar color={"dark"}>
            <ion-button slot={'end'} onClick={() => {this.addAssignment()}} color={'primary'} class="submit-button"> <ion-icon name={"add"}> </ion-icon>
              Add
            </ion-button>
            </ion-toolbar>
          </ion-footer>          
        </ion-grid>
      </ion-content>
    ]
  }
}
