import {Component, Event, EventEmitter, h} from '@stencil/core';

@Component({
  tag: 'assignment-input',
  styleUrl: 'assignment-input.css'
})
export class AssignmentInput {
  @Event() assignmentAdded: EventEmitter;

  clickedTestButton() {
    this.assignmentAdded.emit({
      name: 'The Brand New Assignment ' + Math.floor(Math.random() * 1001),
      due_date: new Date(),
      description: 'This is my assignment description',
      possible_points: 100
    })
  }

  render() {
    return [
      <div style={{width: '100%', height: '300px', backgroundColor: 'lightgray'}}>
        <ion-button onClick={_ => this.clickedTestButton()}>
          CLICK ME!!!
        </ion-button>
      </div>
    ];
  }
}
