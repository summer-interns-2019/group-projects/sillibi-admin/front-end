import {Component, h, Prop} from '@stencil/core';
import {format} from "date-fns";

@Component({
  tag: 'assignment-list',
  styleUrl: 'assignment-list.css'
})
export class AssignmentList {
  @Prop() assignments: any[];
  @Prop() shouldScrollDown: boolean = false;
  private listWrapper : HTMLIonContentElement;
  @Prop({ connect: 'ion-popover-controller' }) popoverController: HTMLIonPopoverControllerElement;

  componentDidRender() {
    if (this.shouldScrollDown) {
      this.listWrapper.scrollToBottom(300);
    }
  }

  async presentPopover(event, assignment) {
    console.log(assignment);
    console.log(event);
    await this.popoverController.componentOnReady();

    const popoverElement = await this.popoverController.create({
      component: 'assignment-popover',
      componentProps: {assignment: {...assignment}},
      event: event,
      showBackdrop: true,
      cssClass: 'pop-over-style'
    });
    await popoverElement.present();
  }

  render() {
    return [
      <ion-content style={{height: 'calc(100vh - 70px - 300px)'}} color="light"
                   ref={el => this.listWrapper = el as HTMLIonContentElement} >
        {this.renderContent()}
      </ion-content>
    ];
  }

  renderContent() {
    return this.assignments.map((assignment) => {
      return [
        <ion-card style={{marginTop: "10px", marginBottom: '20px', backgroundColor: "white"}}
                  onClick={ event => this.presentPopover(event, assignment)}>
          <p style={{marginLeft: '16px', marginTop:"5px", marginBottom: '0px', color: 'black',
                     backgroundColor: 'white'}}>
            Due: {format(assignment.due_date, 'MMMM DD, YYYY')}
          </p>
          <ion-item lines={"none"} >
            <ion-label style={{marginTop:"0"}}>
              <h2>{assignment.name}</h2>
            </ion-label>
            <ion-note color={"dark"} slot={"end"}>{assignment.possible_points} pts</ion-note>
          </ion-item>
          <ion-item lines={"none"} >
            <p style={{marginTop:"0", maxHeight:"80px", color: 'gray'}} class={"ion-text-wrap"}>
               {assignment.description}
            </p>
          </ion-item>
        </ion-card>
      ];
    });
  }
}
