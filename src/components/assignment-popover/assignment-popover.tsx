import {Component, h, Prop, State, Event, EventEmitter, Element} from '@stencil/core';

@Component({
  tag: 'assignment-popover',
  styleUrl: 'assignment-popover.css'
})

export class AssignmentPopover {
  @Prop() assignments: any;
  @Prop() shouldScrollDown: boolean = false;
  @State() editing: boolean = false;
  @Event() updateAssignment: EventEmitter;
  @Prop() assignment: {name:string, possible_points:string, due_date:string, description: string};
  editedAssignment: {name:string, possible_points:string, due_date:string, description: string};
  oldAssignment: {name:string, possible_points:string, due_date:string, description: string};
  @Element() el: Element;

  clickedTestEditButton() {
    (this.update());
    (this.el.closest('ion-popover') as any).dismiss({...this.assignment}, 'edited-assignment');
  }

  clickedTestDeleteButton() {
    (this.el.closest('ion-popover') as any).dismiss({...this.assignment}, 'deleted-assignment');
  }

  componentWillLoad(){
    this.editedAssignment = this.assignment;
  }

  didEdit(){
    this.oldAssignment = {...this.assignment};
  }

  update(){
    this.updateAssignment.emit({oldAssignment: this.oldAssignment, editedAssignment: this.editedAssignment});
    this.didEdit();
    {console.log(this.assignment)}
  }

  nameChanged(event) {
    this.editedAssignment.name = event.target.value;
  }

  pointsChanged(event) {
    this.editedAssignment.possible_points = event.target.value;
  }

  dateChanged(event) {
    this.editedAssignment.due_date = event.target.value;
  }

  descriptionChanged(event) {
    this.editedAssignment.description = event.target.value;
  }

  render() {
    return [
        <ion-list>
          <ion-item>
            <ion-label position={"floating"}>
              Name
            </ion-label>
            <ion-input onInput={(event) => {this.nameChanged(event)}} value ={this.editedAssignment.name}/>
          </ion-item>
          <ion-item>
            <ion-label position={"floating"}>
              Points
            </ion-label>
            <ion-input onInput={(event) => {this.pointsChanged(event)}} value ={this.editedAssignment.possible_points}/>
          </ion-item>
          <ion-item>
            <ion-label position={"floating"}>
              Due Date
            </ion-label>
            <ion-input onInput={(event) => {this.dateChanged(event)}} value ={this.editedAssignment.due_date}/>
          </ion-item>
          <ion-item>
            <ion-label position={"floating"}>
              Description
            </ion-label>
            <ion-input onInput={(event) => {this.descriptionChanged(event)}} value ={this.editedAssignment.description}/>
          </ion-item>
            <ion-button class = {"ion-padding"} onClick={_ => this.clickedTestEditButton()}>
              SAVE
            </ion-button>
            <ion-button style={{float: "right"}} class={"ion-padding"} color="danger" onClick={_ => this.clickedTestDeleteButton()}>
              <ion-icon name={"trash"}> </ion-icon>
            </ion-button>
        </ion-list>
    ];
  }
}
