import {Component, h, Prop, Event, EventEmitter} from '@stencil/core';

@Component({
  tag: 'course-input',
  styleUrl: 'course-input.css'
})
export class CourseInput {
  name: HTMLIonInputElement;
  number: HTMLIonInputElement;
  section: HTMLIonInputElement;
  term: HTMLIonInputElement;
  instructor: HTMLIonInputElement;
  @Prop() syllabus: any;
  inputsArr() { return [this.name, this.number, this.section, this.term, this.instructor];}
  @Prop() courseInfo: any = null;
  @Event() courseAdded: EventEmitter;


  addCourse() {
    if (this.hasBlanks()) {return;}
    //Make DB Call First
    let course = {
      name: this.name.value,
      number: this.number.value,
      section: this.section.value,
      term: this.term.value,
      instructor: this.instructor.value,
    };
    if (this.courseInfo != null) {
      course['id'] = this.courseInfo.id;
    }
    this.courseAdded.emit(course)
  }

  hasBlanks() {
    let checkArr = this.inputsArr();
    for (let i = 0; i < checkArr.length; i++) {
      if (checkArr[i].value === '') {
        return true;
      }
    }
    return false;
  }

  keyPressEvent(event, index) {
    if (event.key === 'Enter') {
      let inputs = this.inputsArr();
      if (index !== inputs.length - 1) {
        inputs[index + 1].setFocus();
      } else {
        this.addCourse();
      }
    }
  }

  render() {
    return [
      <ion-content color={'light'} >
        <ion-grid>
          <ion-row>
            <ion-col size="1">
            </ion-col>
            <ion-col size="10">
              {this.renderCourseForm()}
            </ion-col>
            <ion-col size="1">
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ]
  }

  makeCourseValObj() {
    let returnObj = {
      name: "",
      number: "",
      section: "",
      term: "",
      instructor: ""
    };
    if (this.courseInfo != null) {
      returnObj = this.courseInfo;
    }
    return returnObj;
  }
  
  renderCourseForm() {
    let valObj = this.makeCourseValObj();
    return [
        <ion-card>
          <ion-list>
            <ion-item>
              <ion-label position={"floating"}>
                Course name
              </ion-label>
              <ion-input
                placeholder={'Business administration'}
                ref={el => this.name = el as HTMLIonInputElement}
                onKeyPress={ event => this.keyPressEvent(event, 0)}
                value={valObj.name}
              >
              </ion-input>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Course number
              </ion-label>
              <ion-input
                placeholder={'e.g., MGMT 100'}
                ref={el => this.number = el as HTMLIonInputElement}
                onKeyPress={ event => this.keyPressEvent(event, 1)}
                value={valObj.number}
              >
              </ion-input>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Section number
              </ion-label>
              <ion-input
                placeholder={'e.g., 001'}
                ref={el => this.section = el as HTMLIonInputElement}
                onKeyPress={ event => this.keyPressEvent(event, 2)}
                value={valObj.section}
              >
              </ion-input>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Term
              </ion-label>
              <ion-input
                placeholder={'Fall 2017'}
                ref={el => this.term = el as HTMLIonInputElement}
                onKeyPress={ event => this.keyPressEvent(event, 3)}
                value={valObj.term}
              >
              </ion-input>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Instructor
              </ion-label>
              <ion-input
                placeholder={'Dr. Sam Birk'}
                ref={el => this.instructor = el as HTMLIonInputElement}
                onKeyPress={ event => this.keyPressEvent(event, 4)}
                value={valObj.instructor}
              >
              </ion-input>
            </ion-item>
            <ion-item lines="none">
              <ion-grid>
                <ion-row style={{height: '60px'}}>
                  <ion-col size="1">
                  </ion-col>
                  <ion-col size="10" text-center={true}>
                    <ion-button
                      onClick={()=>{this.addCourse()}}
                      color={'secondary'}
                      style={{height: '100%', width: '80%'}}
                    >
                      <ion-label style={{fontSize: '20px'}}>
                        SAVE COURSE
                      </ion-label>
                    </ion-button>
                  </ion-col>
                  <ion-col size="1">
                  </ion-col>
                </ion-row>
              </ion-grid>
            </ion-item>
            <ion-item lines={'none'}>
              <p style={{fontSize: '12px'}}>
                Not a syllabus?
              </p>
              <ion-buttons>
                <flag-button syllabus={this.syllabus}/>
              </ion-buttons>
            </ion-item>
          </ion-list>
        </ion-card>
    ];
  }
}
