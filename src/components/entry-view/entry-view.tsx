import {Component, h, State, Prop} from '@stencil/core';
import {AssignmentHttpService} from "../../http_services/assignment.http_service";

@Component({
  tag: 'entry-view',
})
export class EntryView {
  @Prop() entry: any;
  @Prop() number: number;
  @State() course: any;
  @State() assignments: any[] = [];
  @State() show: boolean = false;

  async componentWillLoad(){
    this.assignments = await new AssignmentHttpService().where({syllabus_entry_id: this.entry.id});
  }

  async toggle() {
    this.show = !this.show
  }
  renderShowButton(){
    let label = 'show';
    if (this.assignments.length === 0){return}
    else if (this.show === true) {
      label = 'hide'
    } else {
      label = 'show'
    }
    return [
        <ion-button>
          <ion-label>
            {label}
          </ion-label>
        </ion-button>
    ]
  }
  renderAssignments(){
    if (this.show) {
      let i = 0;
      return this.assignments.map(assignment => {
        i++;
        return [
          <ion-item lines={'none'}>
            <ion-label>
              Assignment #{i}
            </ion-label>
          </ion-item>,
          <ion-item>
            <ion-grid>
              <ion-row>
                <ion-col>
                  <ion-label>
                    <p>
                      Assignment Name
                    </p>
                    <h2>
                      {assignment.name}
                    </h2>
                  </ion-label>
                </ion-col>
                <ion-col>
                  <ion-label>
                    <p>
                      Possible points
                    </p>
                    <h2>
                      {assignment.possible_points}
                    </h2>
                  </ion-label>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-label>
                    <p>
                      Due date
                    </p>
                    <h2>
                      {assignment.due_date}
                    </h2>
                  </ion-label>
                </ion-col>
                <ion-col>
                  <ion-label>
                    <p>
                      Description
                    </p>
                    <h2>
                      {assignment.description}
                    </h2>
                  </ion-label>
                </ion-col>
              </ion-row>
            </ion-grid>
          </ion-item>
        ]
      })
    }
  }

  render() {
    return [
      <ion-card>
        <ion-card-content>
        <ion-list lines={'full'}>
          <ion-item>
            <ion-label>
              <h1>
                Entry #{this.number}
              </h1>
            </ion-label>
              <ion-button slot={'end'}>
                <ion-label>
                  Verify
                </ion-label>
              </ion-button>
          </ion-item>
          <ion-item lines={'none'}>
            <ion-label>
              <h1>
                {this.entry.course.name}
              </h1>
            </ion-label>
          </ion-item>
          <ion-item>
            <ion-grid>
              <ion-row>
                <ion-col>
            <ion-label>
              <p>
                Course Number
              </p>
              <h2>
                {this.entry.course.number}
              </h2>
            </ion-label>
                </ion-col>
                <ion-col>
            <ion-label>
              <p>
                Section
              </p>
              <h2>
                {this.entry.course.section}
              </h2>
            </ion-label>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
            <ion-label>
              <p>
                Term
              </p>
              <h2>
                {this.entry.course.term}
              </h2>
            </ion-label>
                </ion-col>
                <ion-col>
            <ion-label>
              <p>
                Instructor
              </p>
              <h2>
                {this.entry.course.instructor}
              </h2>
            </ion-label>
                </ion-col>
              </ion-row>
            </ion-grid>
          </ion-item>
          <ion-item onClick={()=>{this.toggle()}}>
            <ion-label>
              Assignments
            </ion-label>
            {this.renderShowButton()}
            <ion-note slot={'end'}>
              {this.assignments.length}
            </ion-note>
          </ion-item>
          {this.renderAssignments()}
        </ion-list>
        </ion-card-content>
      </ion-card>
    ]
  }
}
