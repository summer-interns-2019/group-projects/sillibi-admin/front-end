import {Component, Event, EventEmitter, h, Prop, State} from '@stencil/core';
import {SyllabusesHttpService} from "../../http_services/syllabuses.http_service";

@Component({
  tag: 'flag-button',
})
export class FlagButton {
  @Prop() syllabus: any;
  @Event() flagged: EventEmitter;
  @State() flagColor: string = 'medium';
  @Prop({ connect: 'ion-alert-controller' }) alertCtrl: HTMLIonAlertControllerElement;

  componentWillLoad(){
    if (this.syllabus.flagged === true) {
      this.flagColor = 'danger'
    } else {
      this.flagColor = 'medium'
    }
  }

  async toggleFlag() {
    if (this.flagColor === 'medium'){
      let alert = await this.alertCtrl.create({
        message: 'Flag this file?',
        buttons: [
          {
            text: 'Yes',
            role: 'Flag',
            handler: async () => {
              this.flagColor = 'danger';
              this.syllabus = {...this.syllabus, flagged: true};
              await new SyllabusesHttpService().update(this.syllabus);
            }
          }, {
            text: 'Cancel',
            role: 'Cancel'
          }
        ]
      });
      await alert.present();
    } else {
      this.flagColor = 'medium';
      this.syllabus = {...this.syllabus, flagged: false};
      await new SyllabusesHttpService().update(this.syllabus);
    }
  }

  render() {
    return [
        <ion-button onClick={()=>{this.toggleFlag()}}>
          <ion-icon slot={'icon-only'} name={'flag'} color={this.flagColor}/>
        </ion-button>
    ];
  }
}
