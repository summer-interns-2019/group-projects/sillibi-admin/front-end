import {Component, h, State, Prop} from '@stencil/core';
import {SyllabusesHttpService} from "../../http_services/syllabuses.http_service";
import {SyllabusEntriesHttpService} from "../../http_services/syllabus_entries.http_service";

@Component({
  tag: 'syllabus-card-review',
})
export class SyllabusCardReview {
  @Prop() syllabusId;
  syllabus: any;
  @State() courseInfo: any = null;
  entries: any[] = [];

  async componentWillLoad() {
    this.syllabus = await new SyllabusesHttpService().find(this.syllabusId);
    this.entries = await new SyllabusEntriesHttpService().where({syllabus_id: this.syllabus.id});
  }

  renderEntries() {
    if (this.entries.length === 0) {
      return [
        <ion-card>
        <p text-center={true}>
          There are currently no syllabus entries to review.
        </p>
        </ion-card>
      ]
    } else {
    return this.entries.map((entry, index) => {
      return [
        <entry-view entry={entry} number={index+1}/>
        ]
    })
    }
  }

  render() {
    return [
      <ion-split-pane content-id="menu-content" when="xs">
        <ion-menu content-id="menu-content" class="my-custom-menu" style={{maxWidth: '10000px'}}>
          <syllabus-view pdfData={this.syllabus.data}/>
        </ion-menu>
        <ion-content id="menu-content" scrollX={false} scrollY={true}>
          <app-header/>
          {this.renderEntries()}
          <ion-fab vertical="bottom" horizontal="end" slot="fixed">
            <ion-fab-button>
              <ion-icon name="add"/>
            </ion-fab-button>
          </ion-fab>
          <ion-fab vertical="bottom" horizontal="start" slot="fixed">
            <ion-button href={'/home/'}>
              <ion-icon name={'arrow-back'}/> &nbsp; Back to all
            </ion-button>
          </ion-fab>
          <ion-item style={{height: '50px'}} lines={'none'}/>
        </ion-content>
      </ion-split-pane>
    ]
  }
}
