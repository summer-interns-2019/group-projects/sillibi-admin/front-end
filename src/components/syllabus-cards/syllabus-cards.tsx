import {Prop, Component, h } from '@stencil/core';
import {SyllabusesHttpService} from "../../http_services/syllabuses.http_service";


@Component({
  tag: 'syllabus-cards',
})

export class SyllabusCards {
  @Prop() adminCardInfo: any[];

   async componentWillLoad() {
     this.adminCardInfo = await new SyllabusesHttpService().where({});
   }


   renderAdminCards() {
    return this.adminCardInfo.map((card) => {
      return [
            <ion-col size={"12"} size-xs={"12"} size-sm={"6"} size-md={"5"} size-lg={"2"}>
              <ion-card href={`/syllabus-review/${card.id}`} color={"light"}>
                <ion-card-header color={"dark"}>
                  <ion-card-subtitle> {card.id} </ion-card-subtitle>
                </ion-card-header>
                <ion-card-content>
                  <ion-item lines={"none"} color={"light"}>
                    <ion-icon size="small" name="today" slot="start"> </ion-icon>
                    <ion-label>{card.external_course_id}</ion-label>
                  </ion-item>
                </ion-card-content>
              </ion-card>
            </ion-col>
          ];
      });
    }

  render() {
    return [
      <ion-content>
        <ion-grid>
          <ion-row>
            {this.renderAdminCards()}
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
