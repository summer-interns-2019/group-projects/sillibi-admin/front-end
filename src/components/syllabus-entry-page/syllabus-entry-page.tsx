import {Component, h, State, Listen, Event, EventEmitter} from '@stencil/core';
import {SyllabusesHttpService} from "../../http_services/syllabuses.http_service";
import {SyllabusEntriesHttpService} from "../../http_services/syllabus_entries.http_service";
import {CourseHttpService} from "../../http_services/course.http_service";
import {format, compareAsc} from "date-fns";

@Component({
  tag: 'syllabus-entry-page',
  styleUrl: 'syllabus-entry-page.css'
})
export class SyllabusEntryPage {
  private syllabus: any;
  @State() courseInfo: any = null;
  @State() editMode: boolean = false;
  @State() assignments: any[] = [];
  private syllabusEntry: any = undefined;
  shouldScrollDown: boolean = true;
  @Event() doneButtonClicked: EventEmitter;

  @Listen('courseAdded')
  async courseAddedResponse(event) {
    let courseService = new CourseHttpService();
    if (this.syllabusEntry === null || this.syllabusEntry === undefined) {
      this.syllabusEntry = await new SyllabusEntriesHttpService().create({syllabus_id: this.syllabus.id});
      event.detail['syllabus_entry_id'] = this.syllabusEntry.id;
      this.courseInfo= await courseService.create(event.detail);
    } else {
      await courseService.update(event.detail);
      this.courseInfo = event.detail;
    }
    this.editMode = false;
  }

  @Listen('assignmentAdded')
  assignmentAddedResponse(event) {
    this.assignments = [...this.assignments, event.detail];
  }

  @Listen('ionPopoverWillDismiss', {target: 'body'})
  assignmentEditedResponse(event) {
    let indexOfId = this.indexOfAssignment(event.detail.data.id);
    switch(event.detail.role) {
      case 'edited-assignment':
        this.assignments[indexOfId] = event.detail.data;
        break;
      case 'deleted-assignment':
        this.assignments.splice(indexOfId, 1);
        break;
      default:
        return;
    }
    this.shouldScrollDown = false;
    this.assignments = [...this.assignments];
  }

  @Listen('doneButtonClicked')
  async doneButtonClickedReaction() {
    if (this.courseInfo === null) {
      alert('You must add the course information!');
      return;
    }
    if (this.assignments.length === 0) {
      alert('You must add information for assignments!');
      return;
    }
    if (confirm('You are about to submit the Syllabus information.')) {
      let assignments = [...this.assignments];
      assignments.sort((a, b) => {
        return compareAsc(a.due_date, b.due_date)
      });
      for (let i = 0; i < assignments.length; i ++) {
        assignments[i].due_date = format(assignments[i].due_date, "YYYY-MM-DD");
      }
      let syllabusAssignmentsObj = {
        id: this.syllabusEntry.id,
        assignments: assignments,
      };
      await new SyllabusEntriesHttpService().putAssignments(syllabusAssignmentsObj);
    }
  }

  indexOfAssignment(id) {
    for(let i = 0; i < this.assignments.length; i++) {
      if (this.assignments[i].id === id) {
        return i;
      }
    }
    return null;
  }

  @Listen('courseHeaderClicked')
  courseHeaderClickedResponse() {
    this.editMode = true;
  }

  async componentWillLoad() {
    //Make DB Call to get the required syllabus
    this.syllabus = await new SyllabusesHttpService().serve();
  }

  componentDidRender() {
    this.shouldScrollDown = true;
  }
  doneButtonAction() {
    this.doneButtonClicked.emit();
  }

  render() {
    return [
      <ion-split-pane content-id="menu-content" when="xs">
        <ion-menu content-id="menu-content" class="my-custom-menu" style={{maxWidth: '10000px'}}>
          <syllabus-view pdfData={this.syllabus.data}/>
        </ion-menu>
        <ion-content id="menu-content" scrollX={false} scrollY={false}>
          {this.renderCourseHeader()}
          {this.renderInput()}
        </ion-content>
      </ion-split-pane>,
      <ion-fab vertical="bottom" horizontal="start" slot="fixed">
        <ion-button onClick={_ => this.doneButtonAction()}>
          I'm Done &nbsp; <ion-icon name="done-all" />
        </ion-button>
      </ion-fab>
    ]
  }

  renderCourseHeader() {
    // Remove next line if you want header to exist even if there is no course Info
    // if (this.courseInfo === null || this.editMode) {return;}
    let course = this.editMode ? null : this.courseInfo;
    return [
      <app-header courseInfo={course} />
    ];
  }

  renderInput() {
    if (this.courseInfo !== null && !this.editMode) {
      return [
        <assignment-list assignments={[...this.assignments]} shouldScrollDown={this.shouldScrollDown}/>,
        <assignment-form />
      ];
    } else {
      return [ <course-input courseInfo={this.courseInfo} syllabus={this.syllabus}/> ];
    }

  }
}
