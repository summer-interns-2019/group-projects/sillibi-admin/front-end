import {Component, h, Prop, State} from '@stencil/core';

@Component({
  tag: 'syllabus-view',
  styleUrl: 'syllabus-view.css'
})
export class SyllabusView {

  @Prop() pdfData: string;
  private wrapper: HTMLDivElement;
  private doneInputButton: HTMLIonButtonElement;
  private pdfContent: HTMLIonContentElement;
  pdfjsLib = window['pdfjs-dist/build/pdf'];
  PDFObject = window['PDFObject'];
  @State() windowWidth = Math.floor(window.innerWidth / 2);
  @State() windowHeight = window.innerHeight;
  private toolbar: HTMLIonToolbarElement;
  private canvasCount: 0;

  holder() {
    console.log(this.wrapper);
    console.log(this.doneInputButton);
    console.log(this.toolbar);
    console.log(this.canvasCount);
    console.log(this.rtime);
  }

  async componentDidRender() {
    if (this.pdfData === '') { return; }
    if (this.PDFObject.supportsPDFs) {
      this.nativePDF();
    } else {
      this.renderPDF();
    }
  }

  private rtime: any;
  private timeout = false;
  private delta = 300;
  
  getContentHeight() {
    return this.windowHeight + 3;
    //56 is the normal toolbar height
  }

  componentDidLoad() {
    let contentHeight = this.getContentHeight();
    this.pdfContent.style.height = contentHeight + 'px';
    window.onresize = () => {
      this.rtime = new Date();
      if (this.timeout === false) {
        this.timeout = true;
        setTimeout(_ => this.resizeEnd(this), this.delta);
      }
    }
  }

  resizeEnd(input_class) {
    if ((new Date() as any) - input_class.rtime < input_class.delta) {
      setTimeout(_ => input_class.resizeEnd(input_class), input_class.delta);
    } else {
      input_class.timeout = false;
      //Window Resize ended
      input_class.resizeReaction(input_class);
    }
  }

  resizeReaction(input_class) {
    let width = Math.floor(window.innerWidth / 2);
    let height = window.innerHeight;
    if (width === input_class.windowWidth && Math.abs(input_class.windowHeight - height) < 50 ) {return;}
    console.log('Width: ' + width);
    console.log('Height: ' + height);
    input_class.windowWidth = width;
    input_class.windowHeight = height;
    let contenHeight = input_class.getContentHeight();
    input_class.pdfContent.style.height = contenHeight + 'px';
  }

  nativePDF() {
    this.wrapper.style.width = '100%';
    this.wrapper.style.height = (this.getContentHeight()) + 'px';
    let url = 'data:application/pdf;base64,' + this.pdfData;
    this.PDFObject.embed(url, "#pdf-content-wrapper");
    console.log('Should see the pdf');
  }

  cleanWrapper() {
    while(this.wrapper.firstChild) {
      this.wrapper.removeChild(this.wrapper.firstChild);
    }
    this.canvasCount = 0;
  }

  async renderPDF() {
    this.cleanWrapper();

    this.pdfjsLib.GlobalWorkerOptions.workerSrc = '/assets/pdfjs-2.0.943-dist/build/pdf.worker.js';
    // Asynchronous download of PDF
    let loadingTask = this.pdfjsLib.getDocument({data: atob(this.pdfData)});

    let pdf = await loadingTask.promise;
    console.log('PDF loaded');

    this.wrapper.style.paddingBottom = '10px';
    this.wrapper.style.width = '100%';
    // let wrapperHeight = this.windowHeight - this.toolbar.offsetHeight - this.doneInputButton.offsetHeight + 2;
    // this.wrapper.style.height = wrapperHeight + 'px';
    // this.wrapper.style.height = '100%';

    for(let i = 1; i <= pdf.numPages; i++) {
      if (this.canvasCount === pdf.numPages) { this.cleanWrapper(); }
      let page = await pdf.getPage(i);

      // console.log('Page loaded');
      let testGrab = page.getViewport(1);
      let pageWidth = testGrab.viewBox[2];
      let pageHeight = testGrab.viewBox[3];

      let hwRatio = pageHeight / this.windowWidth;
      let ratio = (this.windowWidth - 20) / pageWidth;

      let scale = 5;
      console.log(`Scale is ${scale}`);
      let viewport = page.getViewport(scale);

      let canvas: HTMLCanvasElement = document.createElement('canvas') as HTMLCanvasElement;
      canvas.id = `page-canvas${i}`;
      let context = canvas.getContext('2d');
      canvas.width = viewport.width;
      canvas.height = viewport.height;

      canvas.style.width = (this.windowWidth - 20)+ 'px';
      canvas.style.height = (((this.windowWidth - 20) * hwRatio) * ratio) + 'px';
      canvas.style.marginTop = '10px';
      canvas.style.transform = 'translate(10px, 0)';
      this.wrapper.appendChild(canvas);

      let renderContext = {
        canvasContext: context,
        viewport: viewport
      };
      page.render(renderContext);
      // Statement above returns a object with a promise, renderTask.promise;
      console.log('Page rendered');
      this.canvasCount++;
    }
  }
  
  render() {
    return [
      <ion-content style={{height: '500px'}} ref={el => this.pdfContent = el as HTMLIonContentElement}>
        <div>
          <div ref={el => this.wrapper = el as HTMLDivElement} id="pdf-content-wrapper"
               style={{backgroundColor: 'gainsboro', overflow: 'scroll'}} />
        </div>
      </ion-content>,
    ]
  }
}
