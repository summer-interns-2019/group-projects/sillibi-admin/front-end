export class Helper {
  static objectFilter(obj, fields : string[]) {
    let returnObj = {};
    for (let i = 0; i < fields.length; i++) {
      returnObj[fields[i]] = obj[fields[i]]
    }
    return returnObj;
  }
}
