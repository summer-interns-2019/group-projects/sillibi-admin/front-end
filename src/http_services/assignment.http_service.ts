import {CrudHttpService} from './crud.http_service';

export class AssignmentHttpService extends CrudHttpService {
  constructor() {
    super('assignments');
  }
}
