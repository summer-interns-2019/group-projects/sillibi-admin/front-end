export class CrudHttpService {
  url: string;
  constructor(apiUrl: string) {
    this.url = `https://admin-sillibi.herokuapp.com/${apiUrl}`;
  }

  async where(params: any) {
    const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
    const fullUrl = `${this.url}?${queryString}`;
    return await this.getRequest(fullUrl);
  }

  async find(id: string) {
    const fullUrl = `${this.url}/${id}`;
    return await this.getRequest(fullUrl);
  }

  async update(payload: any) {
    if (payload.id === null || payload.id === undefined) { return; }
    const fullUrl = `${this.url}/${payload.id}`;
    const response = await fetch(fullUrl, {
      method: 'PUT',
      headers: {'Content-Type': 'application/json', 'authorization': localStorage.token},
      mode: 'cors',
      body: JSON.stringify(payload),

    });
    return await response.json();
  }

  async delete(id: number|string = '') {
    const fullUrl = `${this.url}/${id}`;
    const response = await fetch(fullUrl, {
      method: 'DELETE',
      headers: {'Content-Type': 'application/json', 'authorization': localStorage.token},
      mode: 'cors',
    });
    return await response;
  }

  async create(payload: any) {
    const fullUrl = `${this.url}`;
    const response = await fetch(fullUrl, {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'authorization': localStorage.token},
      mode: 'cors',
      body: JSON.stringify(payload),

    });
    if (response.headers.get('authorization') != null){
      localStorage.token = response.headers.get('authorization');
    }
    return await response.json();
  }

  async getRequest(fullUrl) {
    try {
      const response = await fetch(fullUrl, {
        method: 'GET',
        headers: {'Content-Type': 'application/json', 'authorization': localStorage.token},
        mode: 'cors',
      });

      if (response.status === 204) {
        return [];
      } else if([403, 401].includes(response.status)) {
        console.error('not authorized');
        delete localStorage.token;
        location.pathname = '/'
      } else if (response.status === 500) {
        throw new Error("Internal Server Error");
      } else if (response.status === 503) {
        throw new Error("Build in progress");
      } else {
        const data = await response.json();
        return data;
      }
    } catch (error) {
      console.error(error);
      if (location.hash.includes('errors')) { return; }
    }
  }

}
