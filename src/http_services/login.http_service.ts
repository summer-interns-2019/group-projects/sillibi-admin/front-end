import {CrudHttpService} from './crud.http_service';

export class LoginHttpService extends CrudHttpService {
  constructor() {
    super('login');
  }

}
