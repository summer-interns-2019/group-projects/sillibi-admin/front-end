import {CrudHttpService} from './crud.http_service';

export class LogoutHttpService extends CrudHttpService {
  constructor() {
    super('logout');
  }
}
