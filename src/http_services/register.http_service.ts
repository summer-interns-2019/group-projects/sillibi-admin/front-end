import {CrudHttpService} from './crud.http_service';

export class RegisterHttpService extends CrudHttpService {
  constructor() {
    super('signup');
  }
}
