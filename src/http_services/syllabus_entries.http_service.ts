import {CrudHttpService} from './crud.http_service';

export class SyllabusEntriesHttpService extends CrudHttpService {
  constructor() {
    super('syllabus_entries');
  }

  async putAssignments(payload: any) {
    if (payload.id === null || payload.id === undefined) { return; }
    const fullUrl = `${this.url}/${payload.id}/put_assignments`;
    const response = await fetch(fullUrl, {
      method: 'PUT',
      headers: {'Content-Type': 'application/json', 'authorization': localStorage.token},
      mode: 'cors',
      body: JSON.stringify(payload),
    });
    return await response.json();
  }
  // async getAssignments(id: string){
  //   const fullUrl = `${this.url}/${id}/get_assignments`;
  //   return await this.getRequest(fullUrl);
  // }
}
