import {CrudHttpService} from './crud.http_service';

export class SyllabusesHttpService extends CrudHttpService {
  constructor() {
    super('syllabuses');
  }

  async serve() {
    const fullUrl = `${this.url}/serve`;
    return await this.getRequest(fullUrl);
  }
}
